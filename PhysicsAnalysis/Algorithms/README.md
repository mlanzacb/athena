## Common CP Algorithms

You can use these to build your analysis framework. They should provide all the most up to date recommendations from the Combined Performance (CP) groups.

### Policies for developers:

- **Do not** add `ComponentAccumulator`-based configuration here. All configuration should be based on `ConfigBlock`.
