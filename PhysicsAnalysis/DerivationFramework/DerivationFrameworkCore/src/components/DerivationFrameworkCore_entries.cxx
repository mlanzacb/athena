#include "DerivationFrameworkCore/DerivationKernel.h"
#include "DerivationFrameworkCore/CommonAugmentation.h"
#include "DerivationFrameworkCore/TriggerMatchingAugmentation.h"
#include "../GoodRunsListFilterAlgorithm.h"
#include "../LockDecorations.h"

DECLARE_COMPONENT( DerivationFramework::DerivationKernel )
DECLARE_COMPONENT( DerivationFramework::CommonAugmentation )
DECLARE_COMPONENT( DerivationFramework::TriggerMatchingAugmentation  )
DECLARE_COMPONENT( DerivationFramework::GoodRunsListFilterAlgorithm )
DECLARE_COMPONENT( DerivationFramework::LockDecorations )
