/**
 *  * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 *   */

#include "../HGTD_SmearedDigitizationTool.h"

DECLARE_COMPONENT( HGTD_SmearedDigitizationTool )
