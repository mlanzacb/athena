/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../TreeTesterAlg.h"
DECLARE_COMPONENT(MuonVal::MuonTester::TreeTestAlg)