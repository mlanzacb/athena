//
// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "../DeviceMgmtSvc.h"

// Declare the component(s) to Gaudi.
DECLARE_COMPONENT(AthXRT::DeviceMgmtSvc)
