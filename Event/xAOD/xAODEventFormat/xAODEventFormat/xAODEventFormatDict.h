// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODEVENTFORMAT_XAODEVENTFORMATDICT_H
#define XAODEVENTFORMAT_XAODEVENTFORMATDICT_H

// Includes for the dictionary generation:
#include "xAODEventFormat/EventFormat.h"
#include "xAODEventFormat/EventFormatElement.h"
#include "xAODEventFormat/versions/EventFormat_v1.h"

#endif // XAODEVENTFORMAT_XAODEVENTFORMATDICT_H
