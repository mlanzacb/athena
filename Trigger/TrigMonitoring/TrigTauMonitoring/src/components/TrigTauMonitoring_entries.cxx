/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../TrigTauMonitorSingleAlgorithm.h"
#include "../TrigTauMonitorDiTauAlgorithm.h"
#include "../TrigTauMonitorTandPAlgorithm.h"
#include "../TrigTauMonitorTruthAlgorithm.h"
#include "../TrigTauMonitorL1Algorithm.h"

DECLARE_COMPONENT( TrigTauMonitorSingleAlgorithm )
DECLARE_COMPONENT( TrigTauMonitorDiTauAlgorithm )
DECLARE_COMPONENT( TrigTauMonitorTandPAlgorithm )
DECLARE_COMPONENT( TrigTauMonitorTruthAlgorithm )
DECLARE_COMPONENT( TrigTauMonitorL1Algorithm )
