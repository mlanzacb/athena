# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Trigger/TriggerCommon/TriggerMenuMT/python/TriggerAPI/__init__.py

# lines below allow for imports from the package rather than the module
# e.g.: from TriggerMenuMT.TriggerAPI import TriggerAPISession,TriggerType

from .TriggerPeriodData import TriggerPeriodData
from .TriggerAPI import TriggerAPI
from .TriggerAPISession import TriggerAPISession
from .TriggerEnums import TriggerPeriod,TriggerType
